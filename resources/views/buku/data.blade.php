@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="card-title h4">Buku</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        @csrf
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">Kode Buku</label>
                                    <input type="text" class="form-control" value="{{ $request['kode_buku'] ?? "" }}" name="kode_buku">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">Judul Buku</label>
                                    <input type="text" class="form-control" value="{{ $request['judul_buku'] ?? "" }}" name="judul_buku">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <button class="btn btn-primary mt-4">Cari</button>
                                    <a href="/buku" class="btn btn-danger mt-4">Reset</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr class="grey">
                    <a href="{{ route('buku.create') }}" class="btn btn-success">Tambah</a>
                    <table class="table table-bordered table-striped mt-3">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode buku</th>
                                <th>Judul</th>
                                <th>Pengarang</th>
                                <th>Tahun</th>
                                <th>ISBN</th>
                                <th>jumlah halaman</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item->kode_buku }}</td>
                                    <td>{{ $item->judul }}</td>
                                    <td>{{ $item->pengarang }}</td>
                                    <td>{{ $item->tahun }}</td>
                                    <td>{{ $item->isbn }}</td>
                                    <td>{{ $item->jumlah_halaman }}</td>
                                    <td>
                                        <form action="{{ route('buku.destroy', $item->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('buku.edit', $item->id) }}"
                                                class="btn btn-primary">Edit</a>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
