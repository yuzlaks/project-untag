@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="card-title h4">Edit Buku</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('buku.update', $data->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="">Kode buku</label>
                            <input type="text" value="{{ $data->kode_buku }}" placeholder="Kode buku" name="kode_buku" class="form-control">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Judul</label>
                            <input type="text" value="{{ $data->judul }}" placeholder="Judul" name="judul" class="form-control">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Pengarang</label>
                            <input type="text" value="{{ $data->pengarang }}" placeholder="Pengarang" name="pengarang" class="form-control">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Tahun</label>
                            <input type="text" value="{{ $data->tahun }}" placeholder="Tahun" name="tahun" class="form-control">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">ISBN</label>
                            <input type="text" value="{{ $data->isbn }}" placeholder="ISBN" name="isbn" class="form-control">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">jumlah halaman</label>
                            <input type="text" value="{{ $data->jumlah_halaman }}" placeholder="jumlah halaman" name="jumlah_halaman" class="form-control">
                        </div>
                        <button class="btn btn-primary btn-sm mt-3">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
