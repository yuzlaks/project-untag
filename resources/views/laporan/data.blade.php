@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="card-title h4">Laporan Sirkulasi Buku</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="container">
                            <div class="col-4">
                                <label for="">NBI</label>
                                <select name="" class="form-control" id="">
                                    @foreach ($sirkulasi as $item)
                                        <option value="{{ $item->nbi }}">{{ $item->nbi }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-4 mt-2">
                                <label for="">Nama</label>
                                <input type="text" class="form-control nama">
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped mt-3">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode buku</th>
                                <th>Judul</th>
                                <th>Jumlah</th>
                                <th>Tanggal pinjam</th>
                                <th>Tanggal kembali</th>
                                <th>Kondisi</th>
                                <th>Denda</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
