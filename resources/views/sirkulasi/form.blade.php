@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="card-title h4">Tambah Sirkulasi</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('sirkulasi.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">NBI</label>
                            <input class="form-control" type="text" placeholder="nbi" name="nbi">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Tanggal pinjam</label>
                            <input class="form-control tanggal_pinjam" type="date" placeholder="tanggal_pinjam" name="tanggal_pinjam">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Kode buku</label>
                            <select name="kode_buku" id="" class="form-control kode_buku">
                                <option value="" disabled selected>Pilih Buku</option>
                                @foreach ($buku as $item)
                                    <option value="{{ $item->kode_buku }}">{{ $item->kode_buku }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Tanggal kembali</label>
                            <input class="form-control tanggal_kembali" type="date" placeholder="tanggal_kembali" name="tanggal_kembali">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Denda</label>
                            <input class="form-control denda" type="text" placeholder="denda" name="denda">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Kondisi</label>
                            <select name="" class="form-control kondisi" id="">
                                <option selected disabled>Pilih Kondisi</option>
                                <option value="1">Baik</option>
                                <option value="2">Rusak</option>
                            </select>
                            <input type="hidden" name="kondisi" class="send-kondisi">
                        </div>
                        <button class="btn btn-primary btn-sm mt-3">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });
        $(document).ready(function(){
            $('.kode_buku').change(function(){
                var kode_buku = $(this).val();
                $.ajax({
                    url : "{{ url('sirkulasi/check-book') }}",
                    type : "POST",
                    data : {
                        kode_buku : kode_buku
                    },
                    success:function(res){
                        if (res.kondisi) {
                            $('.kondisi').val(res.kondisi);
                            $('.send-kondisi').val(res.kondisi);
                            $('.kondisi').attr('disabled', true);
                        }else{
                            $('.kondisi').val('Pilih Kondisi');
                            $('.kondisi').attr('disabled', false);
                            $('.send-kondisi').val('');
                        }
                    }
                })
            })
            $('.kondisi').change(function(){
                var kondisi = $(this).val();
                $('.send-kondisi').val(kondisi);
            })
            $('.tanggal_kembali').change(function(){
                var tanggal_pinjam = moment($('.tanggal_pinjam').val()).format('Y-MM-DD');
                var tanggal_kembali = moment($(this).val()).format('Y-MM-DD');
                
                $.ajax({
                    url : "{{ url('sirkulasi/get-denda') }}",
                    type : "POST",
                    data : {
                        tanggal_pinjam : tanggal_pinjam,
                        tanggal_kembali : tanggal_kembali
                    },
                    success:function(res){
                        $('.denda').val(res);
                    }
                })
            })
        })
    </script>
@endpush