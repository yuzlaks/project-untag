@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="card-title h4">Sirkulasi</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        @csrf
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">Kode Buku</label>
                                    <input type="text" class="form-control" value="{{ $request['kode_buku'] ?? "" }}" name="kode_buku">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">NBI</label>
                                    <input type="text" class="form-control" value="{{ $request['nbi'] ?? "" }}" name="nbi">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <button class="btn btn-primary mt-4">Cari</button>
                                    <a href="/sirkulasi" class="btn btn-danger mt-4">Reset</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr class="grey">
                    <a href="{{ route('sirkulasi.create') }}" class="btn btn-success">Tambah</a>
                    <table class="table table-bordered table-striped mt-3">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode buku</th>
                                <th>Judul</th>
                                <th>Jumlah</th>
                                <th>Tanggal pinjam</th>
                                <th>Tanggal kembali</th>
                                <th>NBI</th>
                                <th>Kondisi</th>
                                <th>Denda</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item->kode_buku }}</td>
                                    <td>{{ $item->buku->judul ?? "" }}</td>
                                    <td>{{ $item->buku->jumlah_halaman ?? "" }}</td>
                                    <td>{{ date('d-m-Y', strtotime($item->tanggal_pinjam)) }}</td>
                                    <td>{{ date('d-m-Y', strtotime($item->tanggal_kembali)) }}</td>
                                    <td>{{ $item->nbi }}</td>
                                    <td>{{ $item->kondisi == 1 ? "Baik" : "Rusak" }}</td>
                                    <td style="text-align: right">{{ "Rp " . number_format($item->denda,0,',','.')  }}</td>
                                    <td>
                                        <form action="{{ route('sirkulasi.destroy', $item->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('sirkulasi.edit', $item->id) }}"
                                                class="btn btn-primary">Edit</a>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="8"><b>Denda</b></td>
                                <td class="text-danger" style="text-align: right;font-weight: bold">{{ "Rp " . number_format($sum_denda,0,',','.')  }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
