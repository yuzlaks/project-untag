@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="card-title h4">Edit Sirkulasi</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('sirkulasi.update', $data->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="">NBI</label>
                            <input class="form-control" value="{{ $data->nbi }}" type="text" placeholder="nbi" name="nbi">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Tanggal pinjam</label>
                            <input class="form-control" value="{{ $data->tanggal_pinjam }}" type="date" placeholder="tanggal_pinjam" name="tanggal_pinjam">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Kode buku</label>
                            <select name="kode_buku" id="" class="form-control">
                                @foreach ($buku as $item)
                                    <option {{ $data->kode_buku == $item->kode_buku ? "selected" : "" }} value="{{ $item->kode_buku }}">{{ $item->kode_buku }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Tanggal kembali</label>
                            <input class="form-control" value="{{ $data->tanggal_kembali }}" type="date" placeholder="tanggal_kembali" name="tanggal_kembali">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Denda</label>
                            <input class="form-control" value="{{ $data->denda }}" type="text" placeholder="denda" name="denda">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Kondisi</label>
                            <select disabled name="kondisi" class="form-control" id="">
                                <option {{ $data->kondisi == 1 ? "selected" : "" }} value="1">Baik</option>
                                <option {{ $data->kondisi == 2 ? "selected" : "" }} value="2">Rusak</option>
                            </select>
                        </div>
                        <button class="btn btn-primary btn-sm mt-3">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
