<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SirkulasiModel extends Model
{
    use HasFactory;
    protected $table = 'sirkulasi';
    protected $fillable = [
        'tanggal_pinjam',
        'nbi',
        'kode_buku',
        'tanggal_kembali',
        'denda',
        'kondisi'
    ];
    
    public function buku()
    {
        return $this->hasOne(BukuModel::class,'kode_buku','kode_buku');
    }
}
