<?php

namespace App\Http\Controllers;

use App\Models\BukuModel;
use App\Models\SirkulasiModel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SirkulasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $query = SirkulasiModel::with('buku')->orderBy('sirkulasi.id','DESC');
        
        if (isset($_GET['kode_buku'])) {
            $query->where('kode_buku', 'like', "%".$_GET['kode_buku']."%");
        }

        if (isset($_GET['nbi'])) {
            $query->where('nbi', 'like', "%".$_GET['nbi']."%");
        }

        return view('sirkulasi.data', [
            'data' => $query->get(),
            'request' => $_GET,
            'sum_denda' => $query->sum('denda')
        ]);
    }
    
    public function checkBook(Request $request)
    {
        $kode_buku = $request->kode_buku;
        $sirkulasi = SirkulasiModel::where('kode_buku', $kode_buku)->orderBy('id','DESC')->first();
        return response()->json($sirkulasi);
    }

    public function getDenda(Request $request)
    {

        $toDate = Carbon::parse($request->tanggal_kembali);
        $fromDate = Carbon::parse($request->tanggal_pinjam);
  
        $days = $toDate->diffInDays($fromDate);
        $months = $toDate->diffInMonths($fromDate);
        $years = $toDate->diffInYears($fromDate);

        $denda = 0;

        if ($days > 7) {
            $denda = ( (int) $days - 6) * 1000;
        }

        // return response()->json($days);

        echo $denda;

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $buku = BukuModel::orderBy('id','desc')->get();
        return view('sirkulasi.form', [
            'buku' => $buku
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        SirkulasiModel::create($request->all());
        return redirect('sirkulasi');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = SirkulasiModel::where('id', $id)->first();
        $buku = BukuModel::orderBy('id','desc')->get();
        return view('sirkulasi.form-edit', [
            'data' => $data,
            'buku' => $buku,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        SirkulasiModel::where('id', $id)->update($request->except('_token','_method'));
        return redirect('sirkulasi');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        SirkulasiModel::where('id', $id)->delete();
        return redirect()->back();
    }
}
