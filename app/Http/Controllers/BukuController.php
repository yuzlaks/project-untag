<?php

namespace App\Http\Controllers;

use App\Models\BukuModel;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $query = BukuModel::orderBy('id','DESC');
        
        if (isset($_GET['kode_buku'])) {
            $query->where('kode_buku', 'like', "%".$_GET['kode_buku']."%");
        }

        if (isset($_GET['judul_buku'])) {
            $query->where('judul', 'like', "%".$_GET['judul_buku']."%");
        }

        return view('buku.data', [
            'data' => $query->get(),
            'request' => $_GET
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('buku.form');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        BukuModel::create($request->all());
        return redirect('buku');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = BukuModel::where('id', $id)->first();
        return view('buku.form-edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        BukuModel::where('id', $id)->update($request->except('_token','_method'));
        return redirect('buku');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        BukuModel::where('id', $id)->delete();
        return redirect()->back();
    }
}
