<?php

namespace App\Http\Controllers;

use App\Models\SirkulasiModel;
use Illuminate\Http\Request;

class LaporanSirkulasiBukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $sirkulasi = SirkulasiModel::select(['nbi'])->orderBy('nbi','DESC')->groupBy('nbi')->get();
        return view('laporan.data', [
            'sirkulasi' => $sirkulasi
        ]);
    }
}
