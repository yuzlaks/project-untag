<?php

use App\Http\Controllers\BukuController;
use App\Http\Controllers\LaporanSirkulasiBukuController;
use App\Http\Controllers\SirkulasiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::resource('buku', BukuController::class);
Route::resource('sirkulasi', SirkulasiController::class);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/laporan-index', [LaporanSirkulasiBukuController::class, 'index'])->name('laporan');
Route::post('/sirkulasi/check-book', [SirkulasiController::class, 'checkBook']);
Route::post('/sirkulasi/get-denda', [SirkulasiController::class, 'getDenda']);